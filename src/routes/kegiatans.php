<?php
use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

$app = new \Slim\App;   

$app->get('/', function(Request $request, Response $response) {
  echo 'homepage Kegiatan found';
});





$app->post('/login', function (Request $request, Response $response, $arg){

  $_input = $request->getParsedBody();
  
  $db = new db();
  $pdo = $db->connect();
  
  $_data_1 = $_input['email'];
  $_data_2 = $_input['password'];
  
  $sql = "SELECT * FROM mahasiswa WHERE email = '$_data_1'";    
  $user = null;
  try{
    // $db = new db();
    // $pdo = $db->connect();

    $stmt = $pdo->query($sql);
    $row = $stmt->fetch(PDO::FETCH_OBJ);
    
    $user = $row;
    $pdo = null;
    
    if($user!=null){
      if(password_verify($_data_2,$user->password)){
        $response
      ->getBody()
      ->write(json_encode(array( "status" => "200","message" => "success" , "DataUser" => $user)));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(200);   
      }
      else{
      echo json_encode(array( "status" => "500","message" => "Login Denied!") );
    }
      
    }
    else{
      echo json_encode(array( "status" => "500","message" => "Login Denied!") );
    }
    
  } catch(\PDOException $e){
    echo '{"msg": {"resp": '.$e->getMessage().'}}';
  }
  
});


//uri untuk melakukan pemanggilan pada Api
$app->get('/kegiatan', function(Request $request, Response $response){

//query untuk mengambil data 
  $sql = "SELECT * FROM kegiatan";
  
  
// $db dan $pdo untuk koneksi ke db
  $db = new db();
  $pdo = $db->connect();
  
 //untuk eksekusi query
  $stmt = $pdo->query($sql);
  
     //untuk mengambil data semuanya dari hasil query dengan $stmt 
  $kegiatans = $stmt->fetchAll(PDO::FETCH_OBJ);
  
    //menchek kondisi kalau data nya didapatkan, maka akan ditampilkan datanya 
    //kalau datanya ga didapat maka akan menampilkan pesan error
  
  if ($kegiatans){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataKegiatan" => $kegiatans)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
  
}

});



$app->get('/kegiatan/{kegiatan_id}', function(Request $request, Response $response){
  $kegiatan_id = $request->getAttribute('kegiatan_id');

  $sql = "SELECT * FROM kegiatan WHERE kegiatan_id = $kegiatan_id";

  $db = new db();
  $pdo = $db->connect();

  $stmt = $pdo->query($sql);
  $kegiatan = $stmt->fetchAll(PDO::FETCH_OBJ);

  
  if ($kegiatan){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataKegiatan" => $kegiatan)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
  
}


});


// post Kegiatan request
$app->post('/kegiatan', function(Request $request, Response $response, array $args){
  $bph_id = $request->getParam('bph_id');
  $mahasiswa_id = $request->getParam('mahasiswa_id');
  $periode_id = $request->getParam('periode_id');
  $nama_kegiatan = $request->getParam('nama_kegiatan');
  $waktu_kegiatan = $request->getParam('waktu_kegiatan');
  $tempat_kegiatan = $request->getParam('tempat_kegiatan');
  $deskripsi_kegiatan = $request->getParam('deskripsi_kegiatan');
  $foto_kegiatan = $request->getParam('foto_kegiatan');
  $tanggal_berakhir = $request->getParam('tanggal_berakhir');
  $status_kegiatan = $request->getParam('status_kegiatan');
  $penulis = $request->getParam('penulis');
  try{
        //get db object
    $db = new db();
        //connect
    $pdo = $db->connect();
    $sql = "INSERT INTO kegiatan(bph_id, mahasiswa_id, periode_id, nama_kegiatan, waktu_kegiatan, tempat_kegiatan,deskripsi_kegiatan,foto_kegiatan, tanggal_berakhir, status_kegiatan, penulis) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

    $pdo->prepare($sql)->execute([$bph_id, $mahasiswa_id, $periode_id, $nama_kegiatan, $waktu_kegiatan, $tempat_kegiatan, $deskripsi_kegiatan, $foto_kegiatan, $tanggal_berakhir, $status_kegiatan, $penulis]);

    if($pdo) {
      $payload = [
        "status" => 201,
        "message" => "Kegiatan Berhasil Ditambah"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
      $payload = [
        "status" => 500,
        "message" => "Gagal Menambah Data"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
    }
  } catch(\PDOException $e){
   echo '{"error": {"text": '.$e->getMessage().'}}';
 }
});




$app->get('/pengumuman', function(Request $request, Response $response){
    //koneksi ke database
  $sql = "SELECT * FROM pengumuman";

    //mengambil data dari database

  $db = new db();
  
  $db = new db();
  $pdo = $db->connect();

  $stmt = $pdo->query($sql);
  $pengumumans = $stmt->fetchAll(PDO::FETCH_OBJ);
  
  if ($pengumumans){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataPengumuman" => $pengumumans)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
  
}

});



$app->get('/pengumuman/{pengumuman_id}', function(Request $request, Response $response){
  $pengumuman_id = $request->getAttribute('pengumuman_id');

  $sql = "SELECT * FROM pengumuman WHERE pengumuman_id = $pengumuman_id";

  $db = new db();
  $pdo = $db->connect();
  $stmt = $pdo->query($sql);
  $pengumuman = $stmt->fetchAll(PDO::FETCH_OBJ);
  if ($pengumuman){
    $payload = [
      "status" => 200,
      $pengumuman
    ];
    $response
    ->getBody()
    ->write(json_encode($payload));
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
  }
  else {
   $payload = [
    "status" => 404,
    "message" => "Data Tidak ditemukan"
  ];
  $response->getBody()->write(json_encode($payload));
  return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
}
});

// post Pengumuman request
$app->post('/pengumuman', function(Request $request, Response $response, array $args){
  $bph_id = $request->getParam('bph_id');
  $periode_id = $request->getParam('periode_id');
  $detail_pengumuman = $request->getParam('detail_pengumuman');
  $tanggal_akhir_pengumuman = $request->getParam('tanggal_akhir_pengumuman');
  $judul_pengumuman = $request->getParam('judul_pengumuman');
  $foto_pengumuman = $request->getParam('foto_pengumuman');

  try{
        //get db object
    $db = new db();
        //connect
    $pdo = $db->connect();
    $sql = "INSERT INTO pengumuman(bph_id, periode_id, detail_pengumuman, tanggal_akhir_pengumuman, judul_pengumuman ,foto_pengumuman) VALUES (?,?,?,?,?,?)";

    $pdo->prepare($sql)->execute([$bph_id, $periode_id, $detail_pengumuman, $tanggal_akhir_pengumuman, $judul_pengumuman, $foto_pengumuman]);

    if($pdo) {
      $payload = [
        "status" => 201,
        "message" => "Pengumuman Berhasil Ditambah"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
      $payload = [
        "status" => 500,
        "message" => "Gagal Menambah Data"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
    }
  } catch(\PDOException $e){
   echo '{"error": {"text": '.$e->getMessage().'}}';
 }
});


$app->get('/kas', function(Request $request, Response $response){
  //koneksi ke database
  $sql = "SELECT * FROM uang_kas";
    //mengambil data dari database
  $db = new db();
  $pdo = $db->connect();

  $stmt = $pdo->query($sql);
  $kas = $stmt->fetchAll(PDO::FETCH_OBJ);
  
  if ($kas){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataKas" => $kas)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
  
}

});

$app->get('/kas/{uang_kas_id}', function(Request $request, Response $response){
  $uang_kas_id = $request->getAttribute('uang_kas_id');

  $sql = "SELECT * FROM uang_kas WHERE uang_kas_id = $uang_kas_id";

  $db = new db();
  $pdo = $db->connect();

  $stmt = $pdo->query($sql);
  $kass = $stmt->fetchAll(PDO::FETCH_OBJ);

  if ($kass){
    $payload = [
      "status" => 200,
      $kass
    ];
    $response
    ->getBody()
    ->write(json_encode($payload));
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
  }
  else {
    
   $payload = [
    "status" => 404,
    "message" => "Data Tidak ditemukan"
  ];
  $response->getBody()->write(json_encode($payload));
  return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
}
});

$app->post('/kas', function(Request $request, Response $response, array $args){
  $bph_id = $request->getParam('bph_id');
  $periode_id = $request->getParam('periode_id');
  $jumlah_bayar = $request->getParam('jumlah_bayar');
  $tanggal_bayar = $request->getParam('tanggal_bayar');
  $tanggal_berakhir = $request->getParam('tanggal_berakhir');
  $deskripsi_pembayaran = $request->getParam('deskripsi_pembayaran');
  $status_pembayaran = $request->getParam('status_pembayaran');
  $mahasiswa_id = $request->getParam('mahasiswa_id');
  
  try{
        //get db object
    $db = new db();
        //connect
    $pdo = $db->connect();
    $sql = "INSERT INTO uang_kas(bph_id, periode_id, jumlah_bayar, tanggal_bayar, tanggal_berakhir ,deskripsi_pembayaran, status_pembayaran, mahasiswa_id ) VALUES (?,?,?,?,?,?,?,?)";

    $pdo->prepare($sql)->execute([$bph_id, $periode_id, $jumlah_bayar, $tanggal_bayar, $tanggal_berakhir, $deskripsi_pembayaran, $status_pembayaran ,$mahasiswa_id]);
    $pdo = null;

  } catch(\PDOException $e){
   echo '{"error": {"text": '.$e->getMessage().'}}';
 }
});

$app->get('/mahasiswa', function(Request $request, Response $response){
  $sql = "SELECT * FROM mahasiswa";
    //mengambil data dari database
  $db = new db();
  $pdo = $db->connect();
  $stmt = $pdo->query($sql);
  $mahasiswas = $stmt->fetchAll(PDO::FETCH_OBJ);

  if ($mahasiswas){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataMahasiswa" => $mahasiswas)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
  
}


});



$app->get('/mahasiswa/{mahasiswa_id}', function(Request $request, Response $response){
  $mahasiswa_id = $request->getAttribute('mahasiswa_id');

  $sql = "SELECT * FROM mahasiswa WHERE mahasiswa_id = $mahasiswa_id";

  $db = new db();
  $pdo = $db->connect();
  $stmt = $pdo->query($sql);
  $mahasiswa = $stmt->fetchAll(PDO::FETCH_OBJ);
  
  if ($mahasiswa){
    $payload = [
      "status" => 200,
      $mahasiswa
    ];
    $response
    ->getBody()
    ->write(json_encode($payload));
    return $response->withHeader('Content-Type', 'application/json')->withStatus(200);

  }
  else {
    
   $payload = [
    "status" => 404,
    "message" => "Data Kegiatan Tidak ditemukan"
  ];
  $response->getBody()->write(json_encode($payload));
  return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
}
});



// post mahasiswa request
$app->post('/mahasiswa', function(Request $request, Response $response, array $args){
  $nama = $request->getParam('nama');
  $username = $request->getParam('username');
  $password = $request->getParam('password');
  $nim = $request->getParam('nim');
  $kelas = $request->getParam('kelas');
  $angkatan = $request->getParam('angkatan');
  $alamat = $request->getParam('alamat');
  $email = $request->getParam('email');
  $status_mahasiswa = $request->getParam('status_mahasiswa');
  $tempat_lahir = $request->getParam('tempat_lahir');
  $tanggal_lahir = $request->getParam('tanggal_lahir');
  $role = $request->getParam('role');
  $foto_mahasiswa = $request->getParam('foto_mahasiswa');
  try{
        //get db object
    $db = new db();
        //connect
    $pdo = $db->connect();
    $sql = "INSERT INTO mahasiswa(nama, username, password, nim, kelas, angkatan, alamat, email, status_mahasiswa, tempat_lahir, tanggal_lahir, role, foto_mahasiswa ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

    $pdo->prepare($sql)->execute([ $nama, $username, $password, $nim, $kelas, $angkatan, $alamat, $email, $status_mahasiswa, $tempat_lahir, $tanggal_lahir, $role, $foto_mahasiswa ]);

        //echo '{"notice": {"text": "kuisioner '.$kegiatan_id .' has been just added now"}}';
    if($pdo) {
      $payload = [
        "status" => 201,
        "message" => "Mahasiswa Berhasil Ditambah"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
      $payload = [
        "status" => 500,
        "message" => "Gagal Menambah Data"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
    }
  } catch(\PDOException $e){
   echo '{"error": {"text": '.$e->getMessage().'}}';
 }
});



$app->get('/bph', function(Request $request, Response $response){
  $sql = "SELECT * FROM bph inner join  mahasiswa on bph.mahasiswa_id = mahasiswa.mahasiswa_id inner join periode on periode.periode_id = bph.periode_id";

  $db = new db();
  $pdo = $db->connect();
  $stmt = $pdo->query($sql);
  $bph = $stmt->fetchAll(PDO::FETCH_OBJ);
  
  if ($bph){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataBph" => $bph)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
}

});


$app->get('/birth/{birth_date}', function(Request $request, Response $response){
  $tanggal_lahir = $request->getAttribute('birth_date');
  $sql = "SELECT * FROM mahasiswa WHERE tanggal_lahir LIKE '%$tanggal_lahir%'";

  $db = new db();
  $pdo = $db->connect();
  $stmt = $pdo->query($sql);
  $mahasiswa = $stmt->fetchAll(PDO::FETCH_OBJ);
  if ($mahasiswa){
   $response
   ->getBody()
   ->write(json_encode(array( "status" => "200  ","message" => "success" , "DataBirth" => $mahasiswa)));
   return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
 }
 else {
  echo json_encode(array( "status" => "404","message" => " Data Tidak ditemukan!") );
}
    
});



//get a post kuesioner request
$app->post('/kuisioner', function(Request $request, Response $response, array $args){
  $kegiatan_id = $request->getParam('kegiatan_id');
  $mahasiswa_id = $request->getParam('mahasiswa_id');
  $jab1 = $request->getParam('jab1');
  $jab2 = $request->getParam('jab2');
  $jab3 = $request->getParam('jab3');
  $jab4 = $request->getParam('jab4');

  try{
        //get db object
    $db = new db();
        //connection_aborted()
    $pdo = $db->connect();
    $sql = "INSERT INTO kuisioner_kegiatan(kegiatan_id, mahasiswa_id, jab1, jab2, jab3, jab4) VALUES (?,?,?,?,?,?)";
    $pdo->prepare($sql)->execute([$kegiatan_id, $mahasiswa_id, $jab1, $jab2, $jab3, $jab4]);
    
      // echo json_encode(array( "status" => "200  ","message" => "Tanggapan berhasil ditambahkan!"));
    if($pdo) {
      $payload = [
        "status" => 201,
        "message" => "Kuisioner Berhasil Ditambah"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
    } else {
      $payload = [
        "status" => 500,
        "message" => "Gagal Menambah Data"
      ];
      $response->getBody()->write(json_encode($payload));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(500);
    }
    
  } catch(\PDOException $e){
   echo '{"error": {"text": '.$e->getMessage().'}}';
 }
});


$app->run();
